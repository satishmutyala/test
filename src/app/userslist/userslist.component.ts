import { Component, OnInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { ViewComponent } from './view/view.component';
import { MatDialog } from '@angular/material';
import { DeleteComponent } from './delete/delete.component';
import { CreateEditComponent } from './create-edit/create-edit.component';
import { PolicyService } from '../policy.service';

export interface user {
  firstName: string;
  lastName: number;
  email: number;
  mobile: number;
}
@Component({
  selector: 'app-userslist',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './userslist.component.html',
  styleUrls: ['./userslist.component.scss']
})
export class UserslistComponent  {
  displayedColumns = ['PolicyNumber', 'Amount', 'Description', 'Action' ];
  dataSource = new MatTableDataSource<any>();
  // users: UserData[] = [];
  policies: any[] = [];

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private ref: ChangeDetectorRef,private dialog: MatDialog, private policyService: PolicyService) {
    // for (let i = 1; i <= 5; i++) { this.users.push(User(i)); }
    // this.dataSource = new MatTableDataSource(this.users);
    this.getdata();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.getdata();
  }

  ngOnInit() {
     this.getdata();
  }
  getdata() {
    this.policyService.getPolicies().subscribe((data: any[]) => {
      console.log(data);
      this.policies = [];
      this.policies = data;
      this.dataSource = new MatTableDataSource(this.policies);
  });
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  view(val: any) {
    const viewDialog = this.dialog.open(ViewComponent, {
      data: { titel: 'Payment Type', Data: val },
      width: '500px'
    });
  }
  deletePolicy(policyId: any)  {
    const deleteCodeDialogRef = this.dialog.open(DeleteComponent, {
      data: { deleteContext: 'Delete' }
    });
    deleteCodeDialogRef.afterClosed().subscribe((response: any) => {
      if (response.delete) {
        this.policyService.deletePolicy(policyId).subscribe((ret) => {
          this.policyService.getPolicies().subscribe((data: any[]) => {
            console.log(data);
            this.policies = [];
            this.policies = data;
            this.dataSource = new MatTableDataSource(data);
            this.ref.detectChanges();
        });
          console.log('Policy deleted: ', ret);
    });
      }
    });
  }
 updatePolicy(policy: any) {
  const viewDialog = this.dialog.open(CreateEditComponent, {
    data: { titel: 'Edit', Data: policy },
    width: '500px'
  });
  viewDialog.afterClosed().subscribe((response: any) => {
    console.log(response);
    if (response.data) {
      console.log(response);
      const obj = {
        id: policy.id,
        num: policy.num,
        amount: response.data.value.amount,
        clientId: policy.clientId,
        userId: policy.userId,
        description: response.data.value.description
      };
      this.policyService.updatePolicy(obj).subscribe((ret) => {
        this.policyService.getPolicies().subscribe((data: any[]) => {
          console.log(data);
          this.policies = [];
          this.policies = data;
          this.dataSource = new MatTableDataSource(data);
          this.ref.detectChanges();
      });
        console.log('Policy Updated: ', ret);
  });
    }
});
}
  createuser(val: any) {
  const viewDialog = this.dialog.open(CreateEditComponent, {
    data: { titel: 'Add', Data: val },
    width: '500px'
  });
  viewDialog.afterClosed().subscribe((response: any) => {
    console.log(response);
    if (response.data) {
      console.log(response);
      const obj = {
        id: 0,
        num: response.data.value.num,
        amount: response.data.value.amount,
        clientId: 0,
        userId: 0,
        description: response.data.value.description
      };
      this.policyService.createPolicy(obj).subscribe((ret) => {
        console.log('Policy created: ', ret);
        this.policyService.getPolicies().subscribe((data: any[]) => {
          console.log(data);
          this.policies = [];
          this.policies = data;
          this.dataSource = new MatTableDataSource(data);
          this.ref.detectChanges();
      });
  });
    }
});
}


}

