import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-create-edit',
  templateUrl: './create-edit.component.html',
  styleUrls: ['./create-edit.component.scss']
})
export class CreateEditComponent implements OnInit {
  UsersForm: FormGroup;
  EmployeesData: any = [];
  filterData: any = '';
  titel: any = 'Add';
  constructor( private formBuilder: FormBuilder, private route: ActivatedRoute,
               public dialogRef: MatDialogRef<CreateEditComponent>,
               @Inject(MAT_DIALOG_DATA) public obj: any) {
    this.getForm();
    console.log(obj);
    if (obj.titel === 'Edit') {
      this.titel = 'Edit User';
      this.getEmployeeEditData(obj.Data);
    }
  }
  private getForm() {
    this.UsersForm = this.formBuilder.group({
      num: ['', Validators.required],
      amount: ['',Validators.required],
      description: ['', Validators.required]
    });
  }
  private getEmployeeEditData(obj: any) {
    this.UsersForm.controls['num'].setValue(obj.num);
    this.UsersForm.controls['amount'].setValue(obj.amount);
    this.UsersForm.controls['description'].setValue(obj.description);
  }
  ngOnInit() {
  }

}
