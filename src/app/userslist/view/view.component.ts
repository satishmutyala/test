import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  userDetails: any = [];
  constructor(private route: ActivatedRoute, private router: Router, public dialog: MatDialog,
              public dialogRef: MatDialogRef<ViewComponent>, @Inject(MAT_DIALOG_DATA) public obj: any) {
     this.userDetails = obj.Data;
   }

  ngOnInit() {
    console.log(this.userDetails)
  }

}
